package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"msg": "hello from api :D, new commit here dude BARU BANGET 2",
		})
	})

	r.GET("/me", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"msg": "this is me new",
		})
	})

	r.GET("/halo", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"msg": "Halo dunia",
		})
	})

	r.Run()
}
